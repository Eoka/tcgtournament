﻿using ModelLibrary.Models.Utils;
using System;
using System.Collections.Generic;

namespace ModelLibrary.Models.Events
{
    public class TournamentArgs : EventArgs
    {
        public IList<StringWrapper> players { get; set; }
        public int RoundAmount { get; set; }
        public int RoundLength { get; set; }
    }
}
