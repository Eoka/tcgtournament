﻿using ModelLibrary.Models.Tournaments;
using System.Collections.Generic;
using System.Data.Entity;

namespace ModelLibrary.EntityFramework
{
    public class EFDBContext : DbContext
    {
        public EFDBContext(string connectionString) : base(connectionString)
        {

        }

        public EFDBContext() : this("")
        {

        }

        public DbSet<Player> Players
        {
            get;
            set;
        }

        public DbSet<Tournament> Tournaments
        {
            get;
            set;
        }

        public DbSet<Match> Matches
        {
            get;
            set;
        }

        public DbSet<Round> Rounds
        {
            get;
            set;
        }
        

        protected override void OnModelCreating(DbModelBuilder builder)
        {

            builder.Entity<Player>().HasKey<string>(p => p.Pseudo);
            builder.Entity<Player>().Ignore<int[]>(p => p.Score);

            builder.Entity<Tournament>().HasKey<int>(t => t.Id);
            builder.Entity<Tournament>().HasMany<Player>(t => t.Players).WithMany();
            builder.Entity<Tournament>().Ignore<Dictionary<Player, int[]>>(t => t.Scoreboard);
            builder.Entity<Tournament>().HasMany<Round>(t => t.Rounds).WithRequired();
            builder.Entity<Tournament>().Ignore<int>(t => t.RoundAmount);
            builder.Entity<Tournament>().Ignore<int>(t => t.RoundLength);

            builder.Entity<Round>().HasMany<Match>(r => r.Matches).WithOptional();
            builder.Entity<Round>().HasKey<int>(r => r.Id);

            builder.Entity<Match>().HasRequired<Player>(m => m.Player1);
            builder.Entity<Match>().HasOptional<Player>(m => m.Player2);
            builder.Entity<Match>().HasKey<int>(m => m.Id);

        }
    }
}
