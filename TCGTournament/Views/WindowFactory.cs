﻿using TCGTournament.Views.Interfaces;

namespace TCGTournament.Views
{
    public class WindowFactory
    {
        public static IWindow GetWindow(string name)
        {
            IWindow window;

            switch (name)
            {
                case "InitializeTournament":
                    window = new InitializeTournament();
                    break;
                case "RoundViewer":
                    window = new RoundViewer();
                    break;
                case "Historic":
                    window = new Historic();
                    break;
                case "Scoreboard":
                    window = new Scoreboard();
                    break;
                default:
                    window = null;
                    break;
            }

            return window;
        }
    }
}
