﻿using System;
using System.Collections.Generic;

namespace TCGTournament.AppController
{
    public class ApplicationController : IApplicationController
    {
        private readonly IDictionary<string, Controller> controllers = new Dictionary<string, Controller>();

        public void Register(Controller controller)
        {
            controllers[GetControllerName(controller.GetType())] = controller;
            controller.AppController = this;
        }

        private string GetControllerName(Type type)
        {
            var name = type.Name;
            return name.Substring(0, name.IndexOf("Controller"));
        }

        public ICollection<string> ControllersName
        {
            get { return controllers.Keys; }
        }

        public void NavigateTo(string controllerName, object args)
        {
            if (!controllers.ContainsKey(controllerName))
            {
                throw new ControllerNotFoundException(controllerName);
            }

            controllers[controllerName].HandleNavigation(args);
        }
    }
}
