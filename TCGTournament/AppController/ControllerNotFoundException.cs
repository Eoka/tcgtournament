﻿using System;

namespace TCGTournament.AppController
{
    public class ControllerNotFoundException : Exception
    {
        public ControllerNotFoundException(string name)
            : base(string.Format("Unable to find a controller named {0}. Did you register it?", name))
        {
        }
    }
}
