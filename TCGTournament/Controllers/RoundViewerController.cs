﻿using ModelLibrary.Models.Events;
using ModelLibrary.Models.Utils;
using System.Windows.Forms;
using TCGTournament.AppController;
using TCGTournament.Views;
using TCGTournament.Views.Interfaces;
using System;
using System.Collections.Generic;
using TCGTournament.Views.ViewModels;
using System.Media;
using System.Linq;
using ModelLibrary.Models.Tournaments;
using ModelLibrary.EntityFramework;

namespace TCGTournament.Controllers
{
    public class RoundViewerController : Controller
    {
        private IRoundViewer window;

        private Tournament tournament;
        private RoundGenerator roundGenerator;

        private List<Matchup> matchups;

        private TimerConverter counter;

        public IRoundViewer Window
        {
            get
            {
                return window;
            }
            set
            {
                window = value;
                WindowInit();
            }
        }

        public override void HandleNavigation(object args)
        {
            if (args is TournamentArgs)
            {
                TournamentArgs tournamentArgs = args as TournamentArgs;

                tournament = new Tournament();

                foreach (StringWrapper playerName in tournamentArgs.players)
                {
                    Player player = new Player();
                    player.Pseudo = playerName.Value;
                    player.Score = new int[] { 0, 0 };

                    tournament.AddPlayer(player);
                }

                tournament.RoundAmount = tournamentArgs.RoundAmount;
                tournament.RoundLength = tournamentArgs.RoundLength;
                
                counter = new TimerConverter() { Count = 0 };

                roundGenerator = new RoundGenerator(tournament);

                InitNewRound();

                this.window.DisableHistory();
                window.Show();
            }
        }

        private void NextRoundButtonClicked(object sender, EventArgs e)
        {

            if (tournament.Rounds.Count < tournament.RoundAmount)
            {
                Round currentRound = tournament.CurrentRound;

                for (int i = 0; i < matchups.Count; i++)
                {
                    Match currentMatch = currentRound.Matches[i];
                    Matchup currentMatchup = matchups[i];

                    currentMatch.Player1Points = currentMatchup.Player1Score;
                    currentMatch.Player2Points = currentMatchup.Player2Score;
                }
                
                foreach (Match m in this.tournament.CurrentRound.Matches)
                {
                    m.UpdatePlayersScore();
                }

                this.counter.Count = 0;
                this.window.StopTimer();

                this.window.EnableHistory();

                InitNewRound();

                if (tournament.Rounds.Count == tournament.RoundAmount)
                {
                    window.NextRoundButtonLabel = "End Tournament";
                }
            }
            else
            {
                for (int i = 0; i < matchups.Count; i++)
                {
                    Match currentMatch = tournament.CurrentRound.Matches[i];
                    Matchup currentMatchup = matchups[i];

                    currentMatch.Player1Points = currentMatchup.Player1Score;
                    currentMatch.Player2Points = currentMatchup.Player2Score;
                }

                foreach (Match m in this.tournament.CurrentRound.Matches)
                {
                    m.UpdatePlayersScore();
                }

                tournament.AddRound(new Round());

                window.Close();
                RequestNavigationTo("Scoreboard", new HistoryArgs() { Tournament = tournament });
            }


        }

        private void InitNewRound()
        {
            roundGenerator.GenerateNextRound();

            if (!tournament.PlayerAmountIsEven)
                PutByeMatchLast();

            BindData();

            if (!tournament.PlayerAmountIsEven)
                foreach (Match m in tournament.CurrentRound)
                    if (m.Player1.Pseudo == "(bye)")
                    {
                        window.ByePlayerName = m.Player2.Pseudo;
                        break;
                    }
                    else if (m.Player2.Pseudo == "(bye)")
                    {
                        window.ByePlayerName = m.Player1.Pseudo;
                        break;
                    }

            window.RoundNumberLabel = tournament.Rounds.Count;
        }

        private void PutByeMatchLast()
        {
            List<Match> matchList = tournament.CurrentRound.Matches;

            Match byeMatch = matchList.Single(m => m.Player1.Pseudo == "(bye)" || m.Player2.Pseudo == "(bye)");

            matchList.Remove(byeMatch);
            matchList.Add(byeMatch);
        }

        private void BindData()
        {
            matchups = new List<Matchup>();
            window.ClearBinding();
            window.BindToData(matchups);

            foreach (Match m in tournament.Rounds[tournament.Rounds.Count - 1])
            {
                if (!(m.Player1.Pseudo == "(bye)" || m.Player2.Pseudo == "(bye)"))
                    window.AddItemToBinding(new Matchup() { Player1 = m.Player1.Pseudo, Player2 = m.Player2.Pseudo, Player1Score = 0, Player2Score = 0 });
            }
        }

        private void HistoryButtonClicked(object sender, EventArgs e)
        {
            RequestNavigationTo("Historic", new HistoryArgs() { Tournament = tournament });
        }

        private void SearchFieldValueChanged(object sender, StringArgs e)
        {
            throw new NotImplementedException();
        }

        private void FormHasClosed(object sender, FormClosingEventArgs e)
        {
            var newWindow = WindowFactory.GetWindow("RoundViewer") as IRoundViewer;
            Window = newWindow;
        }

        private void WindowInit()
        {
            window.RoundNumberLabel = 1;
            window.searchFieldValueChanged += SearchFieldValueChanged;
            window.historyButtonClicked += HistoryButtonClicked;
            window.nextRoundButtonClicked += NextRoundButtonClicked;
            window.matchupGridViewCellValueChanged += MatchupGridViewCellValueChanged;
            window.FormClosing += FormHasClosed;
            window.startTimerClicked += StartTimerClicked;
            window.timerTick += TimerTicked;
        }

        private void TimerTicked(object sender, EventArgs e)
        {
            if (this.counter.Minutes < this.tournament.RoundLength)
            {
                ++this.counter.Count;
                this.window.RoundTimerLabel = String.Format("{0:D2} : {1:D2}", this.counter.Minutes, this.counter.Seconds);
            }
            else
            {
                this.window.StopTimer();
                SystemSounds.Beep.Play();
            }
        }

        private void StartTimerClicked(object sender, EventArgs e)
        {
            this.window.StartTimer();
        }

        private void MatchupGridViewCellValueChanged(object sender, EventArgs e)
        {
            bool enable = true;

            foreach (Matchup m in matchups)
            {
                if (!m.isScoreSet)
                {
                    if (!(m.Player1 == "(bye)" || m.Player2 == "(bye)"))
                    {
                        enable = false;
                        break;
                    }
                }
            }

            window.NextRoundButtonEnable = enable;
        }
    }
}
