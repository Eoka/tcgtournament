﻿namespace TCGTournament.AppController
{
    public abstract class Controller
    {
        public IApplicationController AppController { get; set; }
        
        public void RequestNavigationTo(string name, object args)
        {
            AppController.NavigateTo(name, args);
        }

        public abstract void HandleNavigation(object args);
    }
}
