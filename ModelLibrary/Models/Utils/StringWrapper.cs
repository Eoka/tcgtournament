﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLibrary.Models.Utils
{
    public class StringWrapper
    {
        private string stringValue;

        public string Value
        {
            get
            {
                return stringValue;
            }

            set
            {
                this.stringValue = value;
            }
        }

        public StringWrapper(string value)
        {
            this.stringValue = value;
        }
    }
}
