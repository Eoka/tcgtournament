﻿using System.Windows.Forms;
using ModelLibrary.Models.Events;
using TCGTournament.AppController;
using TCGTournament.Views;
using TCGTournament.Views.Interfaces;
using System.Collections.Generic;
using TCGTournament.Utils.Validators;
using System;
using ModelLibrary.Models.Utils;

namespace TCGTournament.Controllers
{
    public class InitializeTournamentController : Controller
    {
        private IInitializeTournament window;
        private List<StringWrapper> playerNames;

        public IInitializeTournament Window
        {
            get
            {
                return window;
            }
            set
            {
                window = value;
                WindowInit();
            }
        }

        public InitializeTournamentController()
        {
            playerNames = new List<StringWrapper>();
        }

        public override void HandleNavigation(object args) { }

        private void AddPlayerClicked(object sender, StringArgs e)
        {
            var name = e.Value;
            window.AddItemToBinding(new StringWrapper(name));
        }

        private void PlayerNameValidate(object sender, StringArgs e)
        {
            PlayerNameValidator validator = new PlayerNameValidator() { PlayerName = e.Value };

            PlayerEqualValidator equalvalidator = new PlayerEqualValidator() { Playername = e.Value, Otherplayernames = playerNames };

            if (!validator.IsValid())
            {
                window.AddPlayerButtonEnabled = false;
                window.PlayerNameError = validator.ErrorText;
            }
            else if (!equalvalidator.IsValid())
            {
                window.PlayerNameError = equalvalidator.Errortext;
                window.AddPlayerButtonEnabled = false;

            }
            else
            {
                window.AddPlayerButtonEnabled = true;
                window.PlayerNameError = "";
            }
        }

        private void DataGridViewCellChanged(object sender, StringArgs e)
        {
            int counter = 0;
            for(int i=0; i < playerNames.Count; i++)
            {
                if(playerNames[i].Value == e.Value)
                {
                    counter++;
                    if (counter >= 2)
                    {
                        MessageBox.Show("Player name already exists");
                        window.LaunchTournamentButtonEnabled = false;
                    }
                    else
                    {
                        window.LaunchTournamentButtonEnabled = true;
                    }
                }
            }
        }

        private void LaunchTournamentClicked(object sender, TournamentArgs e)
        {
            if (IsAValidTournament())
            {
                e.players = playerNames;
                RequestNavigationTo("RoundViewer", e);
            }
            else
            {
                this.window.DisplayError("Invalid Tournament : Check if the number of Rounds is inferior to the number of players, there should be at least 5 players and the round length should be positive.");
            }
        }

        private bool IsAValidTournament()
        {
            return this.window.ValidInput();
        }

        private void FormHasClosed(object sender, FormClosingEventArgs e)
        {
            var newWindow = WindowFactory.GetWindow("InitializeTournament") as IInitializeTournament;
            Window = newWindow;
            WindowInit();
        }

        private void WindowInit()
        {
            window.BindToData(playerNames);
            window.addPlayerOnClick += AddPlayerClicked;
            window.playerNameFieldTextChanged += PlayerNameValidate;
            window.launchTournamentOnClick += LaunchTournamentClicked;
            window.FormClosing += FormHasClosed;
            window.DataGridViewCellChanged += DataGridViewCellChanged;


        }
    }
}
