﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCGTournament.Utils.Validators
{
    public interface IValidator
    {
        bool IsValid();
    }
}
