﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Inspired by Mickaël Simonis

namespace ModelLibrary.Models.Tournaments
{
    public class DutchPairingSystem : IPairingAlgorithm
    {
        private Tournament tournament;
        private List<Player> players;
        private int currentRound = 0;
        private int currentMatchPairing = 0;

        private List<Player> availablePlayer;

        private List<Match> matchesConsidered = new List<Match>();

        private Dictionary<Player, List<Player>> associationMap = new Dictionary<Player, List<Player>>();


        public Tournament Tournament
        {
            set
            {
                this.tournament = value;
            }
        }

        public void GenerateNextRound()
        {
            Round r = new Round();
            InitializePlayerList();
            Pairing();
            r.Matches = matchesConsidered;
            foreach (Match m in r.Matches)
            {
                UpdateAssociationMap(m);
            }
            tournament.AddRound(r);
        }

        private void InitializePlayerList()
        {
            if (currentRound++ == 0)
            {
                InitializeForFirstRound();
            }
            players = tournament.Players.ToList();
            players.Sort();
        }

        private void InitializeForFirstRound()
        {
            if (!tournament.PlayerAmountIsEven)
            {
                tournament.AddPlayer(new Player() { Pseudo = "(bye)" });
            }
            foreach (Player p in tournament.Players)
            {
                associationMap.Add(p, new List<Player>());
            }
        }

        private void Pairing()
        {
            matchesConsidered = new List<Match>();
            bool[] isPlaying = new bool[players.Count];
            List<int[]> playersIdPerMatch = new List<int[]>();
            List<int> idFromWhichRewind = new List<int>();
            for (int i = 0; i < players.Count; i++)
            {
                idFromWhichRewind.Add(i);
            }
            for (int i = 0; i < players.Count - 1; i++)
            {
                if (!isPlaying[i])
                {
                    Match m;
                    int j = idFromWhichRewind[i];
                    if (j < players.Count - 1)
                    {
                        // Search for first unplayed match for player i
                        do
                        {
                            j++;
                            if (!isPlaying[j])
                            {
                                m = new Match() { Player1=players[i], Player2=players[j] };
                            }
                            else
                            {
                                m = null;
                            }
                        }
                        while ((m == null || associationMap[m.Player1].Contains(m.Player2)) && j < players.Count - 1);


                        if (m == null || associationMap[m.Player1].Contains(m.Player2)) // if no new match for player i => change previous
                        {
                            // Remove match from match list
                            Match lastMatch = matchesConsidered.Last<Match>();
                            matchesConsidered.Remove(lastMatch);
                            // Set players from last match as free
                            int[] toRemove = playersIdPerMatch.Last<int[]>();
                            isPlaying[toRemove[0]] = false;
                            isPlaying[toRemove[1]] = false;
                            // Remove match from pairing list
                            playersIdPerMatch.Remove(playersIdPerMatch.Last<int[]>());
                            // Rollback to last player
                            i = toRemove[0] - 1;
                        }
                        else
                        {
                            // Add match to match list
                            matchesConsidered.Add(m);
                            // Set players as not free
                            isPlaying[i] = true;
                            isPlaying[j] = true;
                            // Save match to pairing list
                            playersIdPerMatch.Add(new int[] { i, j });
                            // Set value from pair for future rollbacks
                            idFromWhichRewind[i] = j;
                            for (int k = i + 1; k < players.Count; k++)
                            {
                                idFromWhichRewind[k] = k;
                            }
                        }
                    }
                    else
                    {
                        // Remove match from match list
                        Match lastMatch = matchesConsidered.Last<Match>();
                        matchesConsidered.Remove(lastMatch);
                        // Set players from last match as free
                        int[] toRemove = playersIdPerMatch.Last<int[]>();
                        isPlaying[toRemove[0]] = false;
                        isPlaying[toRemove[1]] = false;
                        // Remove match from pairing list
                        playersIdPerMatch.Remove(playersIdPerMatch.Last<int[]>());
                        // Rollback to last player
                        i = toRemove[0] - 1;
                    }
                }
            }
        }
        

        private void UpdateAssociationMap(Match m)
        {
            associationMap[m.Player1].Add(m.Player2);
            associationMap[m.Player2].Add(m.Player1);
        }

    }
}
