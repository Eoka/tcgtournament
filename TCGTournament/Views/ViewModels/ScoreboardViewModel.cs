﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCGTournament.Views.ViewModels
{
    public class ScoreboardViewModel
    {
        [DisplayName("Player Name")]
        public string PlayerName { get; set; }
        [DisplayName("Points")]
        public int PlayerPoints { get; set; }
        [DisplayName("Resistance points")]
        public int PlayerResistancePoints { get; set; }
    }
}
