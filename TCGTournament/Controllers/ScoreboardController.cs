﻿using ModelLibrary.EntityFramework;
using ModelLibrary.Models.Events;
using ModelLibrary.Models.Tournaments;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TCGTournament.AppController;
using TCGTournament.Views;
using TCGTournament.Views.Interfaces;
using TCGTournament.Views.ViewModels;

namespace TCGTournament.Controllers
{
    public class ScoreboardController : Controller
    {
        private IScoreboard window;
        private Tournament tournament;
        private List<ScoreboardViewModel> scores;

        public IScoreboard Window
        {
            get
            {
                return window;
            }

            set
            {
                window = value;
                InitWindow();
            }
        }

        public override void HandleNavigation(object args)
        {
            if (args is HistoryArgs)
            {
                tournament = (args as HistoryArgs).Tournament;

                scores = new List<ScoreboardViewModel>();

                BindData();

                foreach (Player p in tournament.Players)
                {
                    if (p.Pseudo != "(bye)")
                        window.AddItemToBinding(new ScoreboardViewModel()
                        {
                            PlayerName = p.Pseudo,
                            PlayerPoints = p.Score[0],
                            PlayerResistancePoints = p.Score[1]
                        });
                }

                scores.OrderBy(s => s.PlayerPoints).ThenBy(s => s.PlayerPoints);


                window.Show();
            }
        }

        private void BindData()
        {
            window.BindToData(scores);
        }

        private void QuitButtonClicked(object sender, EventArgs e)
        {
            window.Close();
        }

        private void HistoryButtonClicked(object sender, EventArgs e)
        {
            RequestNavigationTo("Historic", new HistoryArgs() { Tournament = tournament });
        }

        private void FormHasClosed(object sender, FormClosingEventArgs e)
        {
            var newWindow = WindowFactory.GetWindow("RoundViewer") as IScoreboard;
            Window = newWindow;
        }

        private void InitWindow()
        {
            window.quitButtonClicked += QuitButtonClicked;
            window.historyButtonClicked += HistoryButtonClicked;
        }
    }
}
