﻿using ModelLibrary.Models.Events;
using ModelLibrary.Models.Tournaments;
using TCGTournament.AppController;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TCGTournament.Views;
using System.Windows.Forms;
using TCGTournament.Views.Interfaces;
using TCGTournament.Views.ViewModels;

namespace TCGTournament.Controllers
{
    public class HistoricController : Controller
    {
        private Tournament tournament;
        private IHistoric window;
        private int currentRound = 0;

        public IHistoric Window
        {
            get
            {
                return window;
            }
            set
            {
                window = value;
                WindowInit();
            }
        }

        public override void HandleNavigation(object args)
        {
            if (args is HistoryArgs)
            {
                HistoryArgs history = args as HistoryArgs;

                this.tournament = history.Tournament;

                BindData();
                this.window.RoundLabel = "1";
                window.Show();
            }
        }

        private void FormHasClosed(object sender, FormClosingEventArgs e)
        {
            var newWindow = WindowFactory.GetWindow("Historic") as IHistoric;
            Window = newWindow;
        }

        private void WindowInit()
        {
            window.FormClosing += FormHasClosed;
            window.nextRoundClick += NextRoundClicked;
            window.previousRoundClick += PreviousRoundClicked;
            this.currentRound = 0;
        }

        private void NextRoundClicked(object sender, EventArgs e)
        {
            if(++currentRound < this.tournament.Rounds.Count - 1)
            {
                BindData();
                this.window.RoundLabel = (this.currentRound + 1).ToString();
            }
            else
            {
                currentRound = this.tournament.Rounds.Count -1;
            }
        }

        private void PreviousRoundClicked(object sender, EventArgs e)
        {
            if (--currentRound >= 0)
            {
                BindData();
                this.window.RoundLabel = (this.currentRound + 1).ToString();
            }
            else
            {
                currentRound = 0;
            }
        }

        private void BindData()
        {
            List<Matchup> matchups = new List<Matchup>();
            window.ClearBinding();
            window.BindToData(matchups);
            foreach (Match m in tournament.Rounds[this.currentRound])
            {
                window.AddItemToBinding(new Matchup() { Player1 = m.Player1.Pseudo, Player2 = m.Player2.Pseudo, Player1Score = m.Player1Points, Player2Score = m.Player2Points });
            }
        }
    }
}
