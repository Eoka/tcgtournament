﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLibrary.Models.Tournaments
{
    public interface IPairingAlgorithm
    {
        Tournament Tournament { set; }

        void GenerateNextRound();

    }
}
