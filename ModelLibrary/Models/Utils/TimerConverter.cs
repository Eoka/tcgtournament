﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLibrary.Models.Utils
{
    public class TimerConverter
    {
        public int Count
        {
            get;
            set;
        }

        public int Seconds
        {
            get
            {
                return Count % 60;
            }
        }

        public int Minutes
        {
            get
            {
                return Count / 60;
            }
        }
    }
}
