﻿using ModelLibrary.Models.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCGTournament.Utils.Validators
{
    public class PlayerNameValidator : IValidator
    {
        private string playerName;

        public string PlayerName
        {
            get
            {
                return playerName;
            }

            set
            {
                playerName = value;
            }
        }

        private string errorText;

        public string ErrorText
        {
            get
            {
                return errorText;
            }

            set
            {
                errorText = value;
            }
        }

        public bool IsValid()
        {
            var error = true;

            if (string.IsNullOrWhiteSpace(playerName))
                if (playerName == "")
                    errorText = "";
                else
                    errorText = "Player name can't be empty";

            else if (!playerName.All(char.IsLetterOrDigit))
                errorText = "Player name can only contain alphanumeric characters";
            
            else if (playerName.Length > 20)
                errorText = "Player name can't exceed 20 characters";
           
            else
                error = false;

            return !error;
        }
    }
}
