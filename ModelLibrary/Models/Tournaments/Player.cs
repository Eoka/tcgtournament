﻿using System;

namespace ModelLibrary.Models.Tournaments
{
    public class Player : IComparable<Player>, IEquatable<Player>
    {
        private int[] score = new int[2];

        public string Pseudo
        {
            get;
            set;
        }

        public int[] Score
        {
            get
            {
                return this.score;
            }
            set
            {
                this.score = value;
            }
        }

        public int CompareTo(Player p)
        {
            if (p.Score[0] != this.Score[0])
            {
                return p.Score[0] - this.Score[0];
            }
            else
            {
                return p.Score[1] - this.Score[1];
            }
        }

        public bool Equals(Player p)
        {
            return p.Pseudo.Equals(this.Pseudo);
        }

        public override bool Equals(Object obj)
        {
            Player p = obj as Player;
            if (p == null) return false;
            return p.Pseudo.Equals(this.Pseudo);
        }

        public override int GetHashCode()
        {
            return this.Pseudo.GetHashCode() + 29;
        }
    }
}
