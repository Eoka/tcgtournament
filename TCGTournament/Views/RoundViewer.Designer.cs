﻿namespace TCGTournament.Views
{
    partial class RoundViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.roundViewerTableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.matchupsDataGridView = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.historyButton = new System.Windows.Forms.Button();
            this.nextRoundButton = new System.Windows.Forms.Button();
            this.timerLbl = new System.Windows.Forms.Label();
            this.timerBtn = new System.Windows.Forms.Button();
            this.roundNumberLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.searchField = new System.Windows.Forms.TextBox();
            this.byeLabel = new System.Windows.Forms.Label();
            this.byePlayerNameLabel = new System.Windows.Forms.Label();
            this.matchupDataGridViewBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.roundTimer = new System.Windows.Forms.Timer(this.components);
            this.roundViewerTableLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matchupsDataGridView)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matchupDataGridViewBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // roundViewerTableLayout
            // 
            this.roundViewerTableLayout.ColumnCount = 3;
            this.roundViewerTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.roundViewerTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.roundViewerTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.roundViewerTableLayout.Controls.Add(this.matchupsDataGridView, 1, 3);
            this.roundViewerTableLayout.Controls.Add(this.tableLayoutPanel1, 1, 4);
            this.roundViewerTableLayout.Controls.Add(this.roundNumberLabel, 1, 1);
            this.roundViewerTableLayout.Controls.Add(this.tableLayoutPanel2, 1, 2);
            this.roundViewerTableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.roundViewerTableLayout.Location = new System.Drawing.Point(0, 0);
            this.roundViewerTableLayout.Name = "roundViewerTableLayout";
            this.roundViewerTableLayout.RowCount = 6;
            this.roundViewerTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.roundViewerTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.roundViewerTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.roundViewerTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.roundViewerTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.roundViewerTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.roundViewerTableLayout.Size = new System.Drawing.Size(535, 467);
            this.roundViewerTableLayout.TabIndex = 0;
            // 
            // matchupsDataGridView
            // 
            this.matchupsDataGridView.AllowUserToAddRows = false;
            this.matchupsDataGridView.AllowUserToDeleteRows = false;
            this.matchupsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.matchupsDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.matchupsDataGridView.Location = new System.Drawing.Point(33, 88);
            this.matchupsDataGridView.Name = "matchupsDataGridView";
            this.matchupsDataGridView.Size = new System.Drawing.Size(464, 321);
            this.matchupsDataGridView.TabIndex = 1;
            this.matchupsDataGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.MatchupsGridViewCellValueChanged);
            this.matchupsDataGridView.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.matchupsDataGridView_DataError);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 6;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.historyButton, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.nextRoundButton, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.timerLbl, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.timerBtn, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(33, 415);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(464, 29);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // historyButton
            // 
            this.historyButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.historyButton.Enabled = false;
            this.historyButton.Location = new System.Drawing.Point(237, 3);
            this.historyButton.Name = "historyButton";
            this.historyButton.Size = new System.Drawing.Size(94, 23);
            this.historyButton.TabIndex = 0;
            this.historyButton.Text = "History";
            this.historyButton.UseVisualStyleBackColor = true;
            this.historyButton.Click += new System.EventHandler(this.HistoryButtonOnClick);
            // 
            // nextRoundButton
            // 
            this.nextRoundButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nextRoundButton.Enabled = false;
            this.nextRoundButton.Location = new System.Drawing.Point(347, 3);
            this.nextRoundButton.Name = "nextRoundButton";
            this.nextRoundButton.Size = new System.Drawing.Size(94, 23);
            this.nextRoundButton.TabIndex = 1;
            this.nextRoundButton.Text = "Next Round";
            this.nextRoundButton.UseVisualStyleBackColor = true;
            this.nextRoundButton.Click += new System.EventHandler(this.NextRoundButtonOnClick);
            // 
            // timerLbl
            // 
            this.timerLbl.AutoSize = true;
            this.timerLbl.Dock = System.Windows.Forms.DockStyle.Left;
            this.timerLbl.Location = new System.Drawing.Point(3, 0);
            this.timerLbl.Name = "timerLbl";
            this.timerLbl.Size = new System.Drawing.Size(42, 29);
            this.timerLbl.TabIndex = 2;
            this.timerLbl.Text = "Timer : ";
            // 
            // timerBtn
            // 
            this.timerBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.timerBtn.Location = new System.Drawing.Point(137, 3);
            this.timerBtn.Name = "timerBtn";
            this.timerBtn.Size = new System.Drawing.Size(94, 23);
            this.timerBtn.TabIndex = 3;
            this.timerBtn.Text = "Start Timer";
            this.timerBtn.UseVisualStyleBackColor = true;
            this.timerBtn.Click += new System.EventHandler(this.TimerBtnOnClick);
            // 
            // roundNumberLabel
            // 
            this.roundNumberLabel.AutoSize = true;
            this.roundNumberLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.roundNumberLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.roundNumberLabel.Location = new System.Drawing.Point(33, 20);
            this.roundNumberLabel.Name = "roundNumberLabel";
            this.roundNumberLabel.Size = new System.Drawing.Size(464, 35);
            this.roundNumberLabel.TabIndex = 3;
            this.roundNumberLabel.Text = "Round ";
            this.roundNumberLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel2.Controls.Add(this.searchField, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.byeLabel, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.byePlayerNameLabel, 2, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(33, 58);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(464, 24);
            this.tableLayoutPanel2.TabIndex = 4;
            // 
            // searchField
            // 
            this.searchField.Dock = System.Windows.Forms.DockStyle.Fill;
            this.searchField.Enabled = false;
            this.searchField.Location = new System.Drawing.Point(3, 3);
            this.searchField.Name = "searchField";
            this.searchField.Size = new System.Drawing.Size(298, 20);
            this.searchField.TabIndex = 1;
            // 
            // byeLabel
            // 
            this.byeLabel.AutoSize = true;
            this.byeLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.byeLabel.Location = new System.Drawing.Point(307, 0);
            this.byeLabel.Name = "byeLabel";
            this.byeLabel.Size = new System.Drawing.Size(54, 24);
            this.byeLabel.TabIndex = 2;
            this.byeLabel.Text = "bye :";
            this.byeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // byePlayerNameLabel
            // 
            this.byePlayerNameLabel.AutoSize = true;
            this.byePlayerNameLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.byePlayerNameLabel.Location = new System.Drawing.Point(367, 0);
            this.byePlayerNameLabel.Name = "byePlayerNameLabel";
            this.byePlayerNameLabel.Size = new System.Drawing.Size(94, 24);
            this.byePlayerNameLabel.TabIndex = 3;
            this.byePlayerNameLabel.Text = "(none)";
            this.byePlayerNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // roundTimer
            // 
            this.roundTimer.Interval = 1000;
            this.roundTimer.Tick += new System.EventHandler(this.TimerOnTick);
            // 
            // RoundViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(535, 467);
            this.Controls.Add(this.roundViewerTableLayout);
            this.Name = "RoundViewer";
            this.Text = "TCG Tournament - Round Viewer";
            this.roundViewerTableLayout.ResumeLayout(false);
            this.roundViewerTableLayout.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matchupsDataGridView)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matchupDataGridViewBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel roundViewerTableLayout;
        private System.Windows.Forms.DataGridView matchupsDataGridView;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button historyButton;
        private System.Windows.Forms.Button nextRoundButton;
        private System.Windows.Forms.Label roundNumberLabel;
        private System.Windows.Forms.BindingSource matchupDataGridViewBindingSource;
        private System.Windows.Forms.Label timerLbl;
        private System.Windows.Forms.Button timerBtn;
        private System.Windows.Forms.Timer roundTimer;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TextBox searchField;
        private System.Windows.Forms.Label byeLabel;
        private System.Windows.Forms.Label byePlayerNameLabel;
    }
}