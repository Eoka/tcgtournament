﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TCGTournament.Views.Interfaces;

namespace TCGTournament.Views
{
    public partial class Scoreboard : Form, IScoreboard
    {
        public Scoreboard()
        {
            InitializeComponent();
        }

        public event EventHandler quitButtonClicked;
        public event EventHandler historyButtonClicked;
        
        public void BindToData(IList data)
        {
            this.scoreboardDgv.DataSource = data;
            this.scoreboardBs.ResetBindings(true);
            this.scoreboardDgv.DataSource = this.scoreboardBs;
        }

        public void AddItemToBinding(object item)
        {
            this.scoreboardBs.Add(item);
        }

        public void ClearBinding()
        {
            this.scoreboardBs.Clear();
        }

        private void HistoryButtonOnClick(object sender, EventArgs e)
        {
            if (historyButtonClicked != null)
                historyButtonClicked(this, e);
        }

        private void QuitButtonOnClick(object sender, EventArgs e)
        {
            if (quitButtonClicked != null)
                quitButtonClicked(this, e);
        }
    }
}
