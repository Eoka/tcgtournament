﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ModelLibrary.Models.Tournaments
{
    public class RoundGenerator
    {
        private Tournament tournament;
        private int currentRound = 0;
        private Random randomGenerator = new Random();

        private IPairingAlgorithm algorithm;

        private List<Player> players;

        private List<Round> allRoundsWithRoundRobin;
        
        public RoundGenerator(Tournament t)
        {
            this.tournament = t;
            algorithm = new DutchPairingSystem() { Tournament = tournament };
        }

        public void GenerateNextRound()
        {
            algorithm.GenerateNextRound();
            //Round r = new Round();

            //if (currentRound < tournament.RoundAmount)
            //{
            //    if (currentRound == 0)
            //        InitAllRoundsRobin();
            //    r.Matches = allRoundsWithRoundRobin[currentRound++].Matches;
            //}
            //this.tournament.AddRound(r);
        }

        private void InitAllRoundsRobin()
        {
            allRoundsWithRoundRobin = new List<Round>();

            if (!tournament.PlayerAmountIsEven)
            {
                tournament.AddPlayer(new Player() { Pseudo = "(bye)" });
            }

            int halfSize = tournament.Players.Count / 2;

            players = new List<Player>();

            players.AddRange(tournament.Players.Skip(halfSize).Take(halfSize));
            players.AddRange(tournament.Players.Skip(1).Take(halfSize - 1).ToArray().Reverse());

            int teamsSize = players.Count;

            for (int round = 0; round < tournament.RoundAmount; round++)
            {
                Round currentRound = new Round();

                int teamIdx = round % teamsSize;

                Match match = new Match() { Player1 = players[teamIdx], Player2 = tournament.Players.ToList()[0] };
                currentRound.Matches.Add(match);

                for (int idx = 1; idx < halfSize; idx++)
                {
                    int p1 = (round + idx) % teamsSize;
                    int p2 = (round + teamsSize - idx) % teamsSize;

                    match = new Match() { Player1 = players[p1], Player2 = players[p2] };
                    currentRound.Matches.Add(match);
                }

                allRoundsWithRoundRobin.Add(currentRound);
            }
        }

        public void NewTournament(Tournament t)
        {
            this.currentRound = 0;
            this.tournament = t;
        }
    }
}
