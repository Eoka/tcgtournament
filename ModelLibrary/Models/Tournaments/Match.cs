﻿using System;

namespace ModelLibrary.Models.Tournaments
{
    public class Match
    {
        private Player player1;
        private Player player2;

        private int player1Points;
        private int player2Points;

        public Player Player1
        {
            get
            {
                return player1;
            }

            set
            {
                player1 = value;
            }
        }

        public Player Player2
        {
            get
            {
                return player2;
            }

            set
            {
                if (player1.Equals(value))
                {
                    throw new Exception("A player can't play vs himself");
                }
                player2 = value;
            }
        }

        public int Player1Points
        {
            get
            {
                return this.player1Points;
            }
            set
            {
                this.player1Points = value;
            }
        }

        public int Player2Points
        {
            get
            {
                return this.player2Points;
            }
            set
            {
                this.player2Points = value;
            }
        }

        public void UpdatePlayersScore()
        {
            if (player1.Pseudo == "(bye)")
            {
                player2.Score[1] += 2;
            }
            else if (player2.Pseudo == "(bye)")
            {
                player1.Score[1] += 2;
            }
            else
            {
                if (player1Points > player2Points)
                {
                    player1.Score[0] += player1Points;
                    player2.Score[1] += player2Points;
                }
                else
                {
                    player2.Score[0] += player2Points;
                    player1.Score[1] += player1Points;
                }
            }
        }

        public int Id
        {
            get;
            set;
        }


    }
}
