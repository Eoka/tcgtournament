﻿using ModelLibrary.Models.Events;
using System;
using System.Collections;

namespace TCGTournament.Views.Interfaces
{
    public interface IInitializeTournament : IWindow, IBindable
    {
        event EventHandler<StringArgs> addPlayerOnClick;
        event EventHandler<StringArgs> playerNameFieldTextChanged;
        event EventHandler<StringArgs> DataGridViewCellChanged;
        event EventHandler<TournamentArgs> launchTournamentOnClick;

        string PlayerNameError { set; }
        bool AddPlayerButtonEnabled { set; }
        bool LaunchTournamentButtonEnabled { set; }
        bool ValidInput();

        void DisplayError(string s);
    }
}
