﻿using ModelLibrary.Models.Tournaments;

namespace ModelLibrary.EntityFramework
{
    public interface TournamentMapper
    {
        void Add(Tournament tournament);
        Tournament GetById(int id);
    }
}
