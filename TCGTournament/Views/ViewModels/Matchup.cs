﻿using System.ComponentModel;

namespace TCGTournament.Views.ViewModels
{
    public class Matchup
    {
        [ReadOnly(true)]
        public string Player1 { get; set; }

        [ReadOnly(true)]
        public string Player2 { get; set; }
        
        private int player1Score;

        public int Player1Score
        {
            get
            {
                return player1Score;
            }

            set
            {
                player1Score = value;
            }
        }

        private int player2Score;

        public int Player2Score
        {
            get
            {
                return player2Score;
            }

            set
            {
                player2Score = value;
            }
        }

        [Browsable(false)]
        public bool isScoreSet { get { return player1Score != 0 || player2Score != 0; } }
    }
}
