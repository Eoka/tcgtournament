﻿using System.Collections.Generic;

namespace TCGTournament.AppController
{
    public interface IApplicationController
    {
        ICollection<string> ControllersName { get; }
        void NavigateTo(string controllerName, object args);
        void Register(Controller controller);
    }
}
