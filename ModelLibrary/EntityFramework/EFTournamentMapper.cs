﻿using System;
using System.Linq;
using ModelLibrary.Models.Tournaments;

namespace ModelLibrary.EntityFramework
{
    public class EFTournamentMapper : TournamentMapper
    {
        private string connectionString;

        public EFTournamentMapper() : this("TournamentDB") { }

        public EFTournamentMapper(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void Add(Tournament tournament)
        {
            if (tournament == null || tournament.Rounds.Count == 0 || tournament.Players.Count < 5)
            {
                throw new Exception("You tried to add an invalid tournament.");
            }
            using (EFDBContext context = new EFDBContext(this.connectionString))
            {
                context.Tournaments.Add(tournament);
                context.SaveChanges();
            }
        }

        public Tournament GetById(int id)
        {
            Tournament result = null;
            if (id < 0)
            {
                throw new Exception("id should be positive.");
            }
            using (EFDBContext context = new EFDBContext(this.connectionString))
            {
                result = context.Tournaments.Include("Players").Include("Rounds.Matches").SingleOrDefault<Tournament>(t => t.Id == id);
                result.UpdateScoreboard();
                foreach(Round r in result.Rounds)
                {
                    foreach(Match m in r.Matches)
                    {
                        m.UpdatePlayersScore();
                    }
                }
                if (result == null)
                {
                    throw new Exception("No tournament found.");
                }
            }
            return result;
        }
    }
}
