﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCGTournament.Views.Interfaces
{
    public interface IBindable
    {
        void BindToData(IList data);
        void AddItemToBinding(object item);
        void ClearBinding();
    }
}
