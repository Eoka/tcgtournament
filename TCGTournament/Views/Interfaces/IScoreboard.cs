﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TCGTournament.Views.ViewModels;

namespace TCGTournament.Views.Interfaces
{
    public interface IScoreboard : IWindow, IBindable
    {
        event EventHandler quitButtonClicked;
        event EventHandler historyButtonClicked;
    }
}
