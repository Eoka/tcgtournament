﻿namespace TCGTournament.Views
{
    partial class InitializeTournament
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mainTableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.addPlayerButton = new System.Windows.Forms.Button();
            this.playerNameField = new System.Windows.Forms.TextBox();
            this.playerDataGridView = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.roundAmountLabel = new System.Windows.Forms.Label();
            this.roundLengthLabel = new System.Windows.Forms.Label();
            this.roundAmountField = new System.Windows.Forms.NumericUpDown();
            this.roundLengthField = new System.Windows.Forms.NumericUpDown();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.launchTournamentButton = new System.Windows.Forms.Button();
            this.playerNameError = new System.Windows.Forms.Label();
            this.titleLabel = new System.Windows.Forms.Label();
            this.playersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mainTableLayout.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.playerDataGridView)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.roundAmountField)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.roundLengthField)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.playersBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // mainTableLayout
            // 
            this.mainTableLayout.ColumnCount = 3;
            this.mainTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.mainTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.mainTableLayout.Controls.Add(this.tableLayoutPanel1, 1, 3);
            this.mainTableLayout.Controls.Add(this.playerDataGridView, 1, 5);
            this.mainTableLayout.Controls.Add(this.tableLayoutPanel2, 1, 6);
            this.mainTableLayout.Controls.Add(this.tableLayoutPanel3, 1, 7);
            this.mainTableLayout.Controls.Add(this.playerNameError, 1, 4);
            this.mainTableLayout.Controls.Add(this.titleLabel, 1, 1);
            this.mainTableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainTableLayout.Location = new System.Drawing.Point(0, 0);
            this.mainTableLayout.Name = "mainTableLayout";
            this.mainTableLayout.RowCount = 9;
            this.mainTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.mainTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.mainTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.mainTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.mainTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.mainTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.mainTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.mainTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.mainTableLayout.Size = new System.Drawing.Size(525, 461);
            this.mainTableLayout.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.Controls.Add(this.addPlayerButton, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.playerNameField, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(38, 63);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(449, 25);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // addPlayerButton
            // 
            this.addPlayerButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.addPlayerButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addPlayerButton.Enabled = false;
            this.addPlayerButton.Location = new System.Drawing.Point(349, 0);
            this.addPlayerButton.Margin = new System.Windows.Forms.Padding(0);
            this.addPlayerButton.Name = "addPlayerButton";
            this.addPlayerButton.Size = new System.Drawing.Size(100, 25);
            this.addPlayerButton.TabIndex = 6;
            this.addPlayerButton.Text = "Add Player";
            this.addPlayerButton.UseVisualStyleBackColor = true;
            this.addPlayerButton.Click += new System.EventHandler(this.AddPlayerOnClick);
            // 
            // playerNameField
            // 
            this.playerNameField.Dock = System.Windows.Forms.DockStyle.Fill;
            this.playerNameField.Location = new System.Drawing.Point(3, 3);
            this.playerNameField.Name = "playerNameField";
            this.playerNameField.Size = new System.Drawing.Size(343, 20);
            this.playerNameField.TabIndex = 1;
            this.playerNameField.TextChanged += new System.EventHandler(this.PlayerNameFieldTextChanged);
            // 
            // playerDataGridView
            // 
            this.playerDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.playerDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.playerDataGridView.ColumnHeadersVisible = false;
            this.playerDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.playerDataGridView.Location = new System.Drawing.Point(38, 114);
            this.playerDataGridView.Name = "playerDataGridView";
            this.playerDataGridView.Size = new System.Drawing.Size(449, 258);
            this.playerDataGridView.TabIndex = 2;
            this.playerDataGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.playerDataGridView_CellValueChanged);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 51F));
            this.tableLayoutPanel2.Controls.Add(this.roundAmountLabel, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.roundLengthLabel, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.roundAmountField, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.roundLengthField, 3, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(38, 378);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(449, 25);
            this.tableLayoutPanel2.TabIndex = 2;
            // 
            // roundAmountLabel
            // 
            this.roundAmountLabel.AutoSize = true;
            this.roundAmountLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.roundAmountLabel.Location = new System.Drawing.Point(3, 0);
            this.roundAmountLabel.Name = "roundAmountLabel";
            this.roundAmountLabel.Size = new System.Drawing.Size(168, 25);
            this.roundAmountLabel.TabIndex = 0;
            this.roundAmountLabel.Text = "Round Amount :";
            this.roundAmountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // roundLengthLabel
            // 
            this.roundLengthLabel.AutoSize = true;
            this.roundLengthLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.roundLengthLabel.Location = new System.Drawing.Point(227, 0);
            this.roundLengthLabel.Name = "roundLengthLabel";
            this.roundLengthLabel.Size = new System.Drawing.Size(168, 25);
            this.roundLengthLabel.TabIndex = 1;
            this.roundLengthLabel.Text = "Round Length (min) :";
            this.roundLengthLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // roundAmountField
            // 
            this.roundAmountField.Location = new System.Drawing.Point(177, 3);
            this.roundAmountField.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.roundAmountField.Name = "roundAmountField";
            this.roundAmountField.Size = new System.Drawing.Size(44, 20);
            this.roundAmountField.TabIndex = 3;
            this.roundAmountField.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // roundLengthField
            // 
            this.roundLengthField.Location = new System.Drawing.Point(401, 3);
            this.roundLengthField.Name = "roundLengthField";
            this.roundLengthField.Size = new System.Drawing.Size(44, 20);
            this.roundLengthField.TabIndex = 4;
            this.roundLengthField.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel3.Controls.Add(this.launchTournamentButton, 1, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(38, 409);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(449, 29);
            this.tableLayoutPanel3.TabIndex = 3;
            // 
            // launchTournamentButton
            // 
            this.launchTournamentButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.launchTournamentButton.Location = new System.Drawing.Point(299, 0);
            this.launchTournamentButton.Margin = new System.Windows.Forms.Padding(0);
            this.launchTournamentButton.Name = "launchTournamentButton";
            this.launchTournamentButton.Size = new System.Drawing.Size(150, 29);
            this.launchTournamentButton.TabIndex = 5;
            this.launchTournamentButton.Text = "Launch Tournament";
            this.launchTournamentButton.UseVisualStyleBackColor = true;
            this.launchTournamentButton.Click += new System.EventHandler(this.LaunchTournamentOnClick);
            // 
            // playerNameError
            // 
            this.playerNameError.AutoSize = true;
            this.playerNameError.Dock = System.Windows.Forms.DockStyle.Fill;
            this.playerNameError.ForeColor = System.Drawing.Color.Red;
            this.playerNameError.Location = new System.Drawing.Point(38, 91);
            this.playerNameError.Name = "playerNameError";
            this.playerNameError.Size = new System.Drawing.Size(449, 20);
            this.playerNameError.TabIndex = 4;
            this.playerNameError.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // titleLabel
            // 
            this.titleLabel.AutoSize = true;
            this.titleLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.titleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titleLabel.Location = new System.Drawing.Point(38, 20);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(449, 30);
            this.titleLabel.TabIndex = 5;
            this.titleLabel.Text = "Create New Tournament";
            this.titleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // InitializeTournament
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(525, 461);
            this.Controls.Add(this.mainTableLayout);
            this.Name = "InitializeTournament";
            this.Text = "TCG Tournament - New Tournament";
            this.mainTableLayout.ResumeLayout(false);
            this.mainTableLayout.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.playerDataGridView)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.roundAmountField)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.roundLengthField)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.playersBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel mainTableLayout;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button addPlayerButton;
        private System.Windows.Forms.TextBox playerNameField;
        private System.Windows.Forms.DataGridView playerDataGridView;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label roundAmountLabel;
        private System.Windows.Forms.Label roundLengthLabel;
        private System.Windows.Forms.NumericUpDown roundAmountField;
        private System.Windows.Forms.NumericUpDown roundLengthField;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button launchTournamentButton;
        private System.Windows.Forms.BindingSource playersBindingSource;
        private System.Windows.Forms.Label playerNameError;
        private System.Windows.Forms.Label titleLabel;
    }
}