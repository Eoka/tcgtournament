﻿using System.Collections.Generic;
using System.Linq;

namespace ModelLibrary.Models.Tournaments
{
    public class Tournament
    {
        private int id;
        private int roundAmount = 3;
        private int roundLength = 0;
        private int roundCounter = 0;
        private List<Round> rounds = new List<Round>();
        private Dictionary<Player, int[]> scoreboard = new Dictionary<Player, int[]>();

        /// <summary>
        /// The scoreboard values are array => TotalPoints , resistancePoints
        /// </summary>
        public Dictionary<Player, int[]> Scoreboard
        {
            get
            {
                return this.scoreboard;
            }
            set
            {
                this.scoreboard = value;
            }
        } //the dictionary

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public List<Round> Rounds
        {
            get
            {
                return this.rounds;
            }
            set
            {
                this.rounds = value;
            }
        }
        public virtual ICollection<Player> Players
        {
            get
            {
                return this.Scoreboard.Keys.ToList<Player>();
            }
            set
            {
                foreach (Player p in value)
                {
                    this.AddPlayer(p);
                }
            }
        }

        public int PlayerAmount { get { return Players.Count; } }

        public bool PlayerAmountIsEven
        {
            get
            {
                return PlayerAmount % 2 == 0 && !Players.Any(p => p.Pseudo == "(bye)");
            }
        }

        public int RoundAmount
        {
            get
            {
                return roundAmount;
            }

            set
            {
                roundAmount = value;
            }
        }

        public int RoundLength
        {
            get
            {
                return roundLength;
            }

            set
            {
                roundLength = value;
            }
        }

        public Round CurrentRound { get { return Rounds[roundCounter-1]; } }

        public void AddRound(Round r)
        {
            this.rounds.Add(r);
            this.roundCounter++;
        }

        public void AddPlayer(Player key)
        {
            Scoreboard.Add(key, new int[2]); //add a entry      
        }

        private void CompleteScoreboardWithFirstRound(Round r)
        {
            foreach (Match m in r)
            {
                if (m.Player2 == null)
                {
                    scoreboard.Add(m.Player1, new int[2] { 0, 2 });
                }
                else
                {
                    if (m.Player1Points > m.Player2Points)
                    {
                        scoreboard.Add(m.Player1, new int[2] { m.Player1Points, 0 });
                        scoreboard.Add(m.Player2, new int[2] { 0, m.Player2Points });
                    }
                    else
                    {
                        scoreboard.Add(m.Player1, new int[2] { 0, m.Player1Points });
                        scoreboard.Add(m.Player2, new int[2] { m.Player2Points, 0 });
                    }
                }

            }
        }

        private void CompleteScoreboardWithOtherRound(Round r)
        {
            foreach (Match m in r)
            {
                if (m.Player2 == null)
                {
                    scoreboard[m.Player1][1] += 2;
                }
                else
                {
                    if (m.Player1Points > m.Player2Points)
                    {
                        scoreboard[m.Player1][0] += m.Player1Points;
                        scoreboard[m.Player2][1] += m.Player2Points;
                    }
                    else
                    {
                        scoreboard[m.Player1][1] += m.Player1Points;
                        scoreboard[m.Player2][0] += m.Player2Points;
                    }
                }

            }
        }

        public void UpdateScoreboard()
        {
            scoreboard.Clear();
            for (int i = 0; i < this.rounds.Count; ++i)
            {
                if (i == 0)
                {
                    CompleteScoreboardWithFirstRound(this.rounds[0]);
                }
                else
                {
                    CompleteScoreboardWithOtherRound(this.rounds[i]);
                }
            }
        }
    }
}
