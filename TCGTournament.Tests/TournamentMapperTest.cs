﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ModelLibrary.EntityFramework;
using System;
using System.Data.SqlClient;
using System.Configuration;
using ModelLibrary.Models.Tournaments;
using System.Data.Entity;
using System.Collections.Generic;
using System.Data.SQLite;

namespace TCGTournament.Tests
{
    [TestClass]
    public class TournamentMapperTest
    {

        private TournamentMapper mapper = new EFTournamentMapper("TCGTest");
        private Tournament tournament;

        [ClassInitialize]
        public static void initialize(TestContext context)
        {
           //Database.SetInitializer<EFDBContext>(new DropCreateDatabaseAlways<EFDBContext>());
        }

        [TestMethod]
        public void CanAddAValidTournament()
        {
            InitializeValidTournament();
            mapper.Add(tournament);
        }

        [TestMethod]
        public void TournamentsAreFetchable()
        {
            InitializeValidTournament();
            mapper.Add(tournament);
            Tournament readTournament = mapper.GetById(tournament.Id);
            Assert.AreEqual(readTournament.Id, tournament.Id, "Id's should be equals after fetch");
        }

        [TestMethod]
        public void TournamentsIdAreUpdated()
        {
            InitializeValidTournament();
            mapper.Add(tournament);
            Assert.AreNotEqual(tournament.Id, 0, "Id should be updated.");
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void ImpossibleToInsertAnTournamentWitouthRound()
        {
            mapper.Add(new Tournament());
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void ImpossibleToInsertNull()
        {
            mapper.Add(new Tournament());
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void ImpossibleToInserTournamentWithoutTheMinimumNumberOfPlayer()
        {
            InitializeInvalidTournament();
            mapper.Add(tournament);
        }

        [TestMethod]
        public void RoundsShouldBeFetchable()
        {
            InitializeValidTournament();
            mapper.Add(tournament);
            Tournament readTournament = mapper.GetById(tournament.Id);
            Assert.AreEqual(tournament.Rounds.Count, readTournament.Rounds.Count, "The read number of rounds should be the same as inserted(1)");
        }

        [TestMethod]
        public void MatchesShouldBeFetchable()
        {
            InitializeValidTournament();
            mapper.Add(tournament);
            Tournament readTournament = mapper.GetById(tournament.Id);
            Assert.AreEqual(tournament.Rounds[0].Matches[0].Player1, readTournament.Rounds[0].Matches[0].Player1, "Player in matches are fetchable");
        }

        [TestMethod]
        public void PlayersInTheDatabaseAreFetchable()
        {
            InitializeValidTournament();
            mapper.Add(tournament);
            Tournament readTournament = mapper.GetById(tournament.Id);
            Assert.AreEqual(tournament.Players.Count, readTournament.Players.Count, "read number of player : "+ readTournament.Players.Count+" should be the same as inserted(5).");
        }


        private void InitializeInvalidTournament()
        {
            this.tournament = new Tournament();
            this.tournament.Rounds.Add(new Round());
            this.tournament.AddPlayer(new Player() { Pseudo = "haha" });
            this.tournament.AddPlayer(new Player() { Pseudo = "hihi" });
            this.tournament.AddPlayer(new Player() { Pseudo = "hahi" });
            this.tournament.AddPlayer(new Player() { Pseudo = "hohi" });
        }

        private void InitializeValidTournament()
        {
            Player p1 = new Player();
            p1.Pseudo = "haha";
            Player p2 = new Player();
            p2.Pseudo = "hihi";
            this.tournament = new Tournament();
            this.tournament.AddPlayer(p1);
            this.tournament.AddPlayer(p2);
            this.tournament.AddPlayer(new Player() { Pseudo = "hahi" });
            this.tournament.AddPlayer(new Player() { Pseudo = "hohi" });
            this.tournament.AddPlayer(new Player() { Pseudo = "hihu" });
            RoundGenerator generator = new RoundGenerator(this.tournament);
            generator.GenerateNextRound();
            generator.GenerateNextRound();
        }

        [TestCleanup]
        public void CleanUp()
        {
            using (var connection = new SQLiteConnection(ConfigurationManager.ConnectionStrings["TCGTest"].ToString()))
            {
                var command = connection.CreateCommand();
                command.CommandText = "DELETE FROM Matches; DELETE FROM Rounds; DELETE FROM Tournaments; DELETE FROM Players; DELETE FROM TournamentPlayers  ";
                connection.Open();
                command.ExecuteNonQuery();
            }
        }

    }
}
