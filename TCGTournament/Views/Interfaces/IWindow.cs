﻿using System.Windows.Forms;

namespace TCGTournament.Views.Interfaces
{
    public interface IWindow
    {
        event FormClosingEventHandler FormClosing;
        void Show();
        void Close();
    }
}
