﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ModelLibrary.Models.Tournaments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCGTournament.Tests
{
    [TestClass]
    public class RoundGeneratorTest
    {
        private RoundGenerator generator;
        private Tournament tournament;
        
        private void Initialize()
        {
            Player p1 = new Player { Pseudo = "Jorn" };
            Player p2 = new Player { Pseudo = "Jonas" };
            Player p3 = new Player { Pseudo = "Simon" };
            Player p4 = new Player { Pseudo = "Dimitri" };
            Player p5 = new Player { Pseudo = "Bart" };
            Player p6 = new Player { Pseudo = "Jean-Ulric" };
            this.tournament = new Tournament();
            this.tournament.AddPlayer(p1);
            this.tournament.AddPlayer(p2);
            this.tournament.AddPlayer(p3);
            this.tournament.AddPlayer(p4);
            this.tournament.AddPlayer(p5);
            this.tournament.AddPlayer(p6);
            this.tournament.RoundAmount = 5;
            generator = new RoundGenerator(this.tournament);
            generator.GenerateNextRound();
            foreach(Match m in this.tournament.Rounds[0])
            {
                m.Player1.Score[0] += 2;
            }
            generator.GenerateNextRound();
            foreach (Match m in this.tournament.Rounds[1])
            {
                m.Player1.Score[0] += 2;
            }
            generator.GenerateNextRound();
            foreach (Match m in this.tournament.Rounds[2])
            {
                m.Player1.Score[0] += 2;
            }
            generator.GenerateNextRound();
            foreach (Match m in this.tournament.Rounds[3])
            {
                m.Player1.Score[0] += 2;
            }
            generator.GenerateNextRound();
            foreach (Match m in this.tournament.Rounds[4])
            {
               m.Player1.Score[0] += 2;
            }
        }

        private void InitializeOdd()
        {
            this.tournament = new Tournament();
            this.tournament.AddPlayer(new Player { Pseudo = "Jorn" });
            this.tournament.AddPlayer(new Player { Pseudo = "Jonas" });
            this.tournament.AddPlayer(new Player { Pseudo = "Simon" });
            this.tournament.AddPlayer(new Player { Pseudo = "Dimitri"});
            this.tournament.AddPlayer(new Player { Pseudo = "Bart" });
            this.tournament.RoundAmount = 4;
            generator = new RoundGenerator(this.tournament);
            generator.GenerateNextRound();
            generator.GenerateNextRound();
            generator.GenerateNextRound();
            generator.GenerateNextRound();
        }

        [TestMethod]
        public void AllPlayersAreAssociatedInFirstRound()
        {
            Initialize();
            List<Player> list = new List<Player>();
            Assert.AreEqual(3, this.tournament.Rounds[0].Count(),"There should be 3 matches per round");
            foreach(Match m in this.tournament.Rounds[0].Matches)
            {
                list.Add(m.Player1);
                if(m.Player2 != null)
                {
                    list.Add(m.Player2);
                }
            }
            Assert.AreEqual(list.Count, this.tournament.Players.Count, "All players should have been associated.");
        }

        [TestMethod]
        public void AllPlayersAreAssociatedInSecondRound()
        {
            Initialize();
            List<Player> list = new List<Player>();
            Assert.AreEqual(3, this.tournament.Rounds[1].Count(), "There should be 3 matches per round");
            foreach (Match m in this.tournament.Rounds[1].Matches)
            {
                list.Add(m.Player1);
                if (m.Player2 != null)
                {
                    list.Add(m.Player2);
                }
            }
            Assert.AreEqual(list.Count, this.tournament.Players.Count, "All players should have been associated.");
        }

        [TestMethod]
        public void AllPlayersAreAssociatedInThirdRound()
        {
            Initialize();
            List<Player> list = new List<Player>();
            Assert.AreEqual(3, this.tournament.Rounds[2].Count(), "There should be 3 matches per round");
            foreach (Match m in this.tournament.Rounds[2].Matches)
            {
                list.Add(m.Player1);
                if (m.Player2 != null)
                {
                    list.Add(m.Player2);
                }
            }
            Assert.AreEqual(list.Count, this.tournament.Players.Count, "All players should have been associated.");
        }

        [TestMethod]
        public void AllPlayersAreAssociatedInFourthRound()
        {
            Initialize();
            List<Player> list = new List<Player>();
            Assert.AreEqual(3, this.tournament.Rounds[3].Count(), "There should be 3 matches per round");
            foreach (Match m in this.tournament.Rounds[3].Matches)
            {
                list.Add(m.Player1);
                if (m.Player2 != null)
                {
                    list.Add(m.Player2);
                }
            }
            Assert.AreEqual(list.Count, this.tournament.Players.Count, "All players should have been associated.");
        }
        [TestMethod]
        public void AllPlayersAreAssociatedInFifthRound()
        {
            Initialize();
            List<Player> list = new List<Player>();
            Assert.AreEqual(3, this.tournament.Rounds[4].Count(), "There should be 3 matches per round");
            foreach (Match m in this.tournament.Rounds[4].Matches)
            {
                list.Add(m.Player1);
                if (m.Player2 != null)
                {
                    list.Add(m.Player2);
                }
            }
            Assert.AreEqual(list.Count, this.tournament.Players.Count, "All players should have been associated.");
        }
    }
}
