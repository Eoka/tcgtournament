﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TCGTournament.Views.Interfaces;

namespace TCGTournament.Views
{
    public partial class Historic : Form, IHistoric
    {
        public Historic()
        {
            InitializeComponent();
        }

        public string RoundLabel
        {
            set
            {
                this.roundLbl.Text = "Historic : Round " + value;
            }
        }

        public event EventHandler nextRoundClick;
        public event EventHandler previousRoundClick;

        public void AddItemToBinding(object item)
        {
            this.matchupGridViewBindingSource.Add(item);
        }

        public void BindToData(IList data)
        {
            this.matchupGridViewBindingSource.DataSource = data;
            this.matchupGridViewBindingSource.ResetBindings(true);
            this.matchupGridView.DataSource = this.matchupGridViewBindingSource;
        }

        public void ClearBinding()
        {
            this.matchupGridViewBindingSource.Clear();
        }

        private void NextRoundClicked(object sender, EventArgs e)
        {
            if (this.nextRoundClick != null)
                this.nextRoundClick(this, e);
        }

        private void previousButton_Click(object sender, EventArgs e)
        {
            if (this.previousRoundClick != null)
                this.previousRoundClick(this, e);
        }
    }
}
