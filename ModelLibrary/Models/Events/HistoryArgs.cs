﻿using ModelLibrary.Models.Tournaments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLibrary.Models.Events
{
    public class HistoryArgs
    {
        public Tournament Tournament { get; set; }
    }
}
