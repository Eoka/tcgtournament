﻿using ModelLibrary.Models.Events;
using ModelLibrary.Models.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using TCGTournament.Views.Interfaces;

namespace TCGTournament.Views
{
    public partial class InitializeTournament : Form, IInitializeTournament
    {

        public string PlayerNameError
        {
            set
            {
                playerNameError.Text = value;
            }
        }

        public bool AddPlayerButtonEnabled
        {
            set
            {
                addPlayerButton.Enabled = value;
            }
        }

        public bool LaunchTournamentButtonEnabled
        {
            set
            {
                launchTournamentButton.Enabled = value;
            }
        }

        public event EventHandler<StringArgs> addPlayerOnClick;
        public event EventHandler<StringArgs> playerNameFieldTextChanged;
        public event EventHandler<TournamentArgs> launchTournamentOnClick;
        public event EventHandler<StringArgs> DataGridViewCellChanged;

        public InitializeTournament()
        {
            InitializeComponent();
        }

        public void BindToData(IList data)
        {
            playersBindingSource.DataSource = data;
            playersBindingSource.ResetBindings(true);
            playerDataGridView.DataSource = playersBindingSource;
        }

        public void AddItemToBinding(object item)
        {
            playersBindingSource.Add(item);
        }

        public void ClearBinding()
        {
            playersBindingSource.Clear();
        }

        private void AddPlayerOnClick(object sender, EventArgs e)
        {
            if (addPlayerOnClick != null)
            {
                addPlayerOnClick(this, new StringArgs() { Value = playerNameField.Text });
                playerNameField.Text = "";
                playerNameField.Focus();
            }
        }

        private void LaunchTournamentOnClick(object sender, EventArgs e)
        {
           var args = new TournamentArgs();

           args.RoundAmount = (int)roundAmountField.Value;
           args.RoundLength = (int)roundLengthField.Value;
           if (launchTournamentOnClick != null)
               launchTournamentOnClick(this, args);

        }

        private void PlayerNameFieldTextChanged(object sender, EventArgs e)
        {
            if (playerNameFieldTextChanged != null)
                playerNameFieldTextChanged(this, new StringArgs() { Value = playerNameField.Text });
        }

        public void DisplayError(string s)
        {
            MessageBox.Show(s, "Wrong argument" , MessageBoxButtons.OK);
        }

        public bool ValidInput()
        {
            return this.roundLengthField.Value > 0 && this.playerDataGridView.RowCount > this.roundAmountField.Value && this.playerDataGridView.RowCount >= 5;
        }

        private void playerDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {

            var editedCell = this.playerDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex];
            if (editedCell.Value == null)
            {
                MessageBox.Show("Sorry, playername can't be empty.");
                launchTournamentButton.Enabled = false;

            }
            else
            {
                launchTournamentButton.Enabled = true;
            }


            if (DataGridViewCellChanged != null)
                DataGridViewCellChanged(this, new StringArgs() { Value = editedCell.Value==null?" " : editedCell.Value.ToString() });
                

        }
    }
}
