﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace ModelLibrary.Models.Tournaments
{
    public class Round : IEnumerable<Match>
    {
        private List<Match> matches = new List<Match>();

        public List<Match> Matches
        {
            get { return matches; }
            set { this.matches = value; }
        }

        public int Id
        {
            get;
            set;
        }

        public IEnumerator<Match> GetEnumerator()
        {
            return matches.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
