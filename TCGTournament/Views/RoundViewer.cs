﻿using System;
using System.Collections;
using System.Windows.Forms;
using ModelLibrary.Models.Events;
using TCGTournament.Views.Interfaces;
using System.Data;


namespace TCGTournament.Views
{
    public partial class RoundViewer : Form, IRoundViewer
    {
        public RoundViewer()
        {
            InitializeComponent();
        }

        public int RoundNumberLabel
        {
            set { roundNumberLabel.Text = "Round " + value; }
        }

        public string RoundTimerLabel
        {
            set { timerLbl.Text = "Timer : " + value; }
        }

        public string SearchFieldValue
        {
            get { return searchField.Text; }
        }

        public string ByePlayerName
        {
            set { byePlayerNameLabel.Text = value; }
        }

        public string NextRoundButtonLabel
        {
            set { nextRoundButton.Text = value; }
        }

        public bool NextRoundButtonEnable
        {
            set { nextRoundButton.Enabled = value; }
        }

        public bool HistoryButtonEnable
        {
            set { nextRoundButton.Enabled = value; }
        }

        public event EventHandler historyButtonClicked;
        public event EventHandler nextRoundButtonClicked;
        public event EventHandler<StringArgs> searchFieldValueChanged;
        public event EventHandler matchupGridViewCellValueChanged;
        public event EventHandler startTimerClicked;
        public event EventHandler timerTick;


        public void BindToData(IList data)
        {
            matchupDataGridViewBindingSource.DataSource = data;
            matchupDataGridViewBindingSource.ResetBindings(true);
            matchupsDataGridView.DataSource = matchupDataGridViewBindingSource;
        }

        public void AddItemToBinding(object item)
        {
            matchupDataGridViewBindingSource.Add(item);
        }

        public void ClearBinding()
        {
            matchupDataGridViewBindingSource.Clear();
        }

        public void StartTimer()
        {
            roundTimer.Start();
        }

        public void StopTimer()
        {
            roundTimer.Stop();
        }

        public void EnableHistory()
        {
            this.historyButton.Enabled = true;
        }
        public void DisableHistory()
        {
            this.historyButton.Enabled = false;
        }

        private void HistoryButtonOnClick(object sender, EventArgs e)
        {
            if (historyButtonClicked != null)
                historyButtonClicked(this, e);
        }

        private void NextRoundButtonOnClick(object sender, EventArgs e)
        {
            if (nextRoundButtonClicked != null)
                nextRoundButtonClicked(this, e);
            nextRoundButton.Enabled = false;
        }

        private void SearchFieldTextChanged(object sender, EventArgs e)
        {
            if (searchFieldValueChanged != null)
                searchFieldValueChanged(this, new StringArgs() { Value = searchField.Text });
        }

        private void MatchupsGridViewCellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (matchupGridViewCellValueChanged != null)
                matchupGridViewCellValueChanged(this, e);
        }

        private void TimerBtnOnClick(object sender, EventArgs e)
        {
            if (startTimerClicked != null)
                startTimerClicked(this, e);
        }

        private void TimerOnTick(object sender, EventArgs e)
        {
            if (timerTick != null)
                timerTick(this, e);
        }

        private void matchupsDataGridView_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show("Only numbers allowed!");
        }
    }
}
