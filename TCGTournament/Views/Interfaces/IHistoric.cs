﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCGTournament.Views.Interfaces
{
    public interface IHistoric : IWindow, IBindable
    {
        string RoundLabel { set; }

        event EventHandler nextRoundClick;
        event EventHandler previousRoundClick;

    }
}
