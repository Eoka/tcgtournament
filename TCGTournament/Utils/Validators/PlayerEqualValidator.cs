﻿using ModelLibrary.Models.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCGTournament.Utils.Validators
{
    public class PlayerEqualValidator : IValidator
    {
        private string playername;

        public string Playername
        {
            get
            {
                return playername;
            }

            set
            {
                playername = value;
            }
        }

        private List<StringWrapper> otherplayernames;

        public List<StringWrapper> Otherplayernames
        {
            get
            {
                return otherplayernames;
            }

            set
            {
                otherplayernames = value;
            }
        }

        private string errortext;

        public string Errortext
        {
            get
            {
                return errortext;
            }

            set
            {
                errortext = value;
            }
        }

        public bool IsValid()
        {
            var error = true;
            
            if (otherplayernames.Any(p=> p.Value == playername))
            {
                errortext = "Name already exists";
                error = false;
            }


            return error;
        }
    }
}
