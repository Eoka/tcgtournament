﻿using System;
using System.Windows.Forms;
using TCGTournament.AppController;
using TCGTournament.Controllers;
using TCGTournament.Views;

namespace TCGTournament
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            SetupApplication();
            Application.Run(SetupControllers());
        }

        private static Form SetupControllers() {
            var appController = new ApplicationController();

            var initializeTournamentController = new InitializeTournamentController() { Window = new InitializeTournament() };
            appController.Register(initializeTournamentController);

            var roundViewerController = new RoundViewerController() { Window = new RoundViewer() };
            appController.Register(roundViewerController);

            var historicController = new HistoricController() { Window = new Historic() };
            appController.Register(historicController);

            var scoreboardController = new ScoreboardController() { Window = new Scoreboard()};
            appController.Register(scoreboardController);

            return initializeTournamentController.Window as Form;
        }

        private static void SetupApplication() {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
        }
    }
}
