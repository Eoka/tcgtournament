﻿using System;

namespace ModelLibrary.Models.Events
{
    public class StringArgs : EventArgs
    {
        public string Value { get; set; }
    }
}
