﻿using ModelLibrary.Models.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCGTournament.Views.Interfaces
{
    public interface IRoundViewer : IWindow, IBindable
    {
        event EventHandler nextRoundButtonClicked;
        event EventHandler historyButtonClicked;
        event EventHandler<StringArgs> searchFieldValueChanged;
        event EventHandler matchupGridViewCellValueChanged;
        event EventHandler startTimerClicked;
        event EventHandler timerTick;

        int RoundNumberLabel { set; }
        string SearchFieldValue { get; }
        string RoundTimerLabel { set; }
        string ByePlayerName { set; }
        string NextRoundButtonLabel { set; }

        bool NextRoundButtonEnable { set; }
        bool HistoryButtonEnable { set; }

        void StartTimer();
        void StopTimer();
        void EnableHistory();
        void DisableHistory();
    }
}
